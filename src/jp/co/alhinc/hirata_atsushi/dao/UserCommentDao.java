package jp.co.alhinc.hirata_atsushi.dao;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.UserComment;
import jp.co.alhinc.hirata_atsushi.exception.SQLRuntimeException;

public class UserCommentDao {

	//コメント一覧取得用。usersとcommentsを結合して参照
	public List<UserComment> selectComments(Connection connection) {

		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    comments.id as id, ");
			sql.append("    comments.text as text, ");
			sql.append("    users.name as name , ");
			sql.append("    user_id, ");
			sql.append("    message_id, ");
			sql.append("    comments.created_date as created_date, ");
			sql.append("    comments.updated_date as updated_date  ");
			sql.append(" FROM comments ");
			sql.append(" INNER JOIN users ");
			sql.append(" ON comments.user_id = users.id ");
			sql.append(" ORDER BY created_date ");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<UserComment> userComments = toUserComments(rs);
			return userComments;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//引数に渡したSELECT文によって参照された、DB上に登録してあるデータを引っ張りだして格納。
	private List<UserComment> toUserComments(ResultSet rs) throws SQLException {

		List<UserComment> userComments = new ArrayList<UserComment>();

		try {

			while (rs.next()) {

				UserComment userComment = new UserComment();
				userComment.setId(rs.getInt("id"));
				userComment.setUserName(rs.getString("name"));
				userComment.setText(rs.getString("text"));
				userComment.setUserId(rs.getInt("user_id"));
				userComment.setMessageId(rs.getInt("message_id"));
				userComment.setCreatedDate(rs.getTimestamp("created_date"));
				userComments.add(userComment);
			}

			return userComments;

		} finally {
			close(rs);
		}
	}
}