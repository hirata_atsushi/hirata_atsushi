package jp.co.alhinc.hirata_atsushi.dao;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.UserMessage;
import jp.co.alhinc.hirata_atsushi.exception.SQLRuntimeException;

public class UserMessageDao {

	//メッセージ一覧取得用
	public List<UserMessage> selectMessages(Connection connection ,String startDate ,String endDate, String category) {

		PreparedStatement ps = null;

		try {

				StringBuilder sql = new StringBuilder();
				sql.append("SELECT ");
				sql.append("    messages.id as id, ");
				sql.append("    title, ");
				sql.append("    text, ");
				sql.append("    category, ");
				sql.append("    user_id, ");
				sql.append("    name, ");
				sql.append("    messages.created_date as created_date ");
				sql.append("    FROM messages ");
				sql.append("    INNER JOIN users ");
				sql.append("    ON messages.user_id = users.id ");
				sql.append("    WHERE messages.created_date BETWEEN ? AND ? ");
				sql.append("    AND category LIKE ? ");
				sql.append("    ORDER BY created_date DESC ");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, startDate);
				ps.setString(2, endDate);
				ps.setString(3, "%" + category + "%");

				ResultSet rs = ps.executeQuery();
				List<UserMessage> userMessages = toUserMessages(rs);
				return userMessages;


		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//引数に渡したSELECT文によって参照された、DB上に登録してあるデータを引っ張りだして格納。
	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> userMessages = new ArrayList<UserMessage>();

		try {

			while (rs.next()) {

				UserMessage userMessage = new UserMessage();
				userMessage.setId(rs.getInt("id"));
				userMessage.setUserName(rs.getString("name"));
				userMessage.setTitle(rs.getString("title"));
				userMessage.setCategory(rs.getString("category"));
				userMessage.setText(rs.getString("text"));
				userMessage.setUserId(rs.getInt("user_id"));
				userMessage.setCreatedDate(rs.getTimestamp("created_date"));

				userMessages.add(userMessage);
			}
			return userMessages;

		} finally {
			close(rs);
		}
	}
}