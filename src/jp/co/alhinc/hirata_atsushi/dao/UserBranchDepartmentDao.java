package jp.co.alhinc.hirata_atsushi.dao;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.UserBranchDepartment;
import jp.co.alhinc.hirata_atsushi.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	//ユーザー一覧取得用。usersとbranches,departmentを結合して参照
	public List<UserBranchDepartment> selectUserBranchDepartments(Connection connection) {

		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    users.id as id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name , ");
			sql.append("    branches.name as branch_name, ");
			sql.append("    departments.name as department_name, ");
			sql.append("    users.is_stopped ");
			sql.append(" FROM users ");
			sql.append(" INNER JOIN branches ");
			sql.append(" ON users.branch_id = branches.id ");
			sql.append(" INNER JOIN departments ");
			sql.append(" ON users.department_id = departments.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userBranchDepartments = toUserBranchDepartments(rs);
			return userBranchDepartments;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//引数に渡したSELECT文によって参照された、DB上に登録してあるデータを引っ張りだして格納。
	private List<UserBranchDepartment> toUserBranchDepartments(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> userBranchDepartments = new ArrayList<UserBranchDepartment>();

		try {

			while (rs.next()) {

				UserBranchDepartment userBranchDepartment = new UserBranchDepartment();
				userBranchDepartment.setId(rs.getInt("id"));
				userBranchDepartment.setAccount(rs.getString("account"));
				userBranchDepartment.setUserName(rs.getString("name"));
				userBranchDepartment.setBranchName(rs.getString("branch_name"));
				userBranchDepartment.setDepartmentName(rs.getString("department_name"));
				userBranchDepartment.setIsStopped(rs.getInt("is_stopped"));
				userBranchDepartments.add(userBranchDepartment);
			}

			return userBranchDepartments;

		} finally {
			close(rs);
		}
	}
}