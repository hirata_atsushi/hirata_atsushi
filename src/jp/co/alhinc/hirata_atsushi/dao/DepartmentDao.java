package jp.co.alhinc.hirata_atsushi.dao;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.Department;
import jp.co.alhinc.hirata_atsushi.exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> selectDepartments(Connection connection) {
		List<Department> departments = new ArrayList<Department>();

		PreparedStatement ps = null;

		try {

			String sql = "SELECT id, name FROM departments ";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
			 Department department = new Department();
			 department.setId(rs.getInt("id"));
			 department.setName(rs.getString("name"));
			 departments.add(department);

			}

			return departments;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}
}
