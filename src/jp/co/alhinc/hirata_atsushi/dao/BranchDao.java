package jp.co.alhinc.hirata_atsushi.dao;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.Branch;
import jp.co.alhinc.hirata_atsushi.exception.SQLRuntimeException;

public class BranchDao {


	public List<Branch> selectBranches(Connection connection)  {

		List<Branch> branches = new ArrayList<Branch>();
		PreparedStatement ps = null;

		try {

			String sql = "SELECT id, name FROM branches ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

			 Branch branch = new Branch();
			 branch.setId(rs.getInt("id"));
			 branch.setName(rs.getString("name"));
			 branches.add(branch);
			}

			return branches;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}
}
