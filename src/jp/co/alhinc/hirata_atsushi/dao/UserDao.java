package jp.co.alhinc.hirata_atsushi.dao;


import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.alhinc.hirata_atsushi.beans.User;
import jp.co.alhinc.hirata_atsushi.exception.NoRowsUpdatedRuntimeException;
import jp.co.alhinc.hirata_atsushi.exception.SQLRuntimeException;

public class UserDao {

	//ユーザー情報登録用
	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;

		try {

				StringBuilder sql = new StringBuilder();
				sql.append("INSERT INTO users ( ");
				sql.append("    account, ");
				sql.append("    password, ");
				sql.append("    name, ");
				sql.append("    branch_id, ");
				sql.append("    department_id, ");
				sql.append("    created_date, ");
				sql.append("    updated_date ");
				sql.append(") VALUES ( ");
				sql.append("    ?, "); // account
				sql.append("    ?, "); // password
				sql.append("    ?, "); // name
				sql.append("    ?, "); // branch_id
				sql.append("    ?, "); // department_id
				sql.append("    CURRENT_TIMESTAMP, "); // created_date
				sql.append("    CURRENT_TIMESTAMP "); // updated_date
				sql.append(")");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getAccount());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getDepartmentId());

				ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//ユーザー情報変更用
	public void update(Connection connection, User user) {

		PreparedStatement ps = null;

		try {

			if ( ! StringUtils.isEmpty(user.getPassword())) {

				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET ");
				sql.append("    account = ?, ");
				sql.append("    password = ?, ");
				sql.append("    name = ?, ");
				sql.append("    branch_id = ?, ");
				sql.append("    department_id = ?, ");
				sql.append("    updated_date = CURRENT_TIMESTAMP ");
				sql.append("WHERE id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getAccount());
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getDepartmentId());
				ps.setInt(6, user.getId());

			} else {

				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET ");
				sql.append("    account = ?, ");
				sql.append("    name = ?, ");
				sql.append("    branch_id = ?, ");
				sql.append("    department_id = ?, ");
				sql.append("    updated_date = CURRENT_TIMESTAMP ");
				sql.append("WHERE id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getAccount());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getDepartmentId());
				ps.setInt(5, user.getId());
			}

				int count = ps.executeUpdate();
				if (count == 0) throw new NoRowsUpdatedRuntimeException();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//ユーザー停止用
	public void updateToStop(Connection connection, int userId) {

		PreparedStatement ps = null;

		try {

				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET ");
				sql.append("    is_stopped = 1, ");
				sql.append("    updated_date = CURRENT_TIMESTAMP ");
				sql.append(" WHERE id =" + userId);

				ps = connection.prepareStatement(sql.toString());

				int count = ps.executeUpdate();
				if (count == 0) throw new NoRowsUpdatedRuntimeException();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//ユーザー復活用
	public void updateToRevival(Connection connection, int userId) {

		PreparedStatement ps = null;

		try {

				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET ");
				sql.append("    is_stopped = 0, ");
				sql.append("    updated_date = CURRENT_TIMESTAMP ");
				sql.append(" WHERE id =" + userId);

				ps = connection.prepareStatement(sql.toString());

				int count = ps.executeUpdate();
				if (count == 0) throw new NoRowsUpdatedRuntimeException();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//重複チェック用
	public User selectExistingUser(Connection connection, String account){

		PreparedStatement ps = null;

		try {

			String sql = "SELECT * FROM users WHERE account = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);

			//入力されたアカウント名を元に既存ユーザーを探すSELECT文を実行
			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);

			if (users.isEmpty()) {
				return null;

			} else  {
				return users.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}


	//setting画面の入力フォームへの表示兼管理者権限フィルター用
	public User selectUser(Connection connection, int userId){

		PreparedStatement ps = null;

		try {

			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);

			if (users.isEmpty()) {
				return null;

			} else  {
				return users.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}


	//ログイン時の照合用
	public User selectToLogin(Connection connection, String account, String password)
			throws SQLRuntimeException {

		PreparedStatement ps = null;

		try {

			String sql = "SELECT * FROM users WHERE account = ?  AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, password);

			//入力されたアカウント名とパスワードを元に既存ユーザーを探すSELECT文を実行
			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);

			if (users.isEmpty()) {
				return null;

			} else {
				return users.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}

	//引数に渡したSELECT文によって参照された、DB上に登録してあるusersデータを引っ張りだして格納。
	private List<User> toUsers(ResultSet rs) throws SQLException {

		List<User> users = new ArrayList<User>();

		try {

			while (rs.next()) {

				User user = new User();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setIsStopped(rs.getInt("is_stopped"));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));

				users.add(user);
			}
			return users;

		} finally {
			close(rs);
		}
	}
}