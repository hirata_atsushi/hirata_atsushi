package jp.co.alhinc.hirata_atsushi.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import jp.co.alhinc.hirata_atsushi.beans.User;
import jp.co.alhinc.hirata_atsushi.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		List<String> errorMessages = new ArrayList<String>();

		User user = new UserService().selectToLogin(account, password);


		if ( ! isValid(user, errorMessages, account, password)) {
			request.setAttribute("account", account);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}


	private boolean isValid(User user, List<String> errorMessages, String account, String password) {


		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		}

		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");

		} else {

			if (user == null) {
				errorMessages.add("アカウントまたはパスワードが誤っています");

			} else if(user.getIsStopped() == 1) {
				errorMessages.add("アカウントまたはパスワードが誤っています");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}