package jp.co.alhinc.hirata_atsushi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import jp.co.alhinc.hirata_atsushi.beans.Message;
import jp.co.alhinc.hirata_atsushi.beans.User;
import jp.co.alhinc.hirata_atsushi.service.MessageService;


@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//sessionを取得。取得した属性値(loginUser)をuserに保持。
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");
        List<String> errorMessages = new ArrayList<String>();

        String title = request.getParameter("title");
        String text = request.getParameter("text");
        String category = request.getParameter("category");

        //受け取った各種パラメーターの中身の有無及び文字数チェック
        if ( ! isValid(title, text, category, errorMessages)) {
        	request.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("title", title);
        	request.setAttribute("text", text);
        	request.setAttribute("category", category);
            request.getRequestDispatcher("message.jsp").forward(request, response);
            return;
        }

        Message message = new Message();
        message.setTitle(title);
        message.setText(text);
        message.setCategory(category);

        //sessionから取得した属性値(loginUser)からuserのidを取得し、messageに一時保管。
        message.setUserId(user.getId());

        //DBに保存
        new MessageService().insert(message);
        response.sendRedirect("./");
    }

    //値を正しい形式で取得できているかのバリデーション用
    private boolean isValid(String title, String text, String category, List<String> errorMessages) {


    	if(StringUtils.isEmpty(title)) {
    		errorMessages.add("件名を入力してください");

    	} else if (30 < title.length()) {
    		errorMessages.add("件名は30文字以下で入力してください");

    	} else if(title.matches("^( |　)+$")) {
    		errorMessages.add("件名を入力してください");
        }


        if (StringUtils.isEmpty(text)) {
            errorMessages.add("本文を入力してください");

        } else if (1000 < text.length()) {
            errorMessages.add("本文は1000文字以下で入力してください");

        } else if(text.matches("^( |　)+$")) {
			errorMessages.add("本文を入力してください");
        }


        if(StringUtils.isEmpty(category)) {
        	errorMessages.add("カテゴリを入力してください");
        } else if (10 < category.length()) {
            errorMessages.add("カテゴリは10文字以下で入力してください");

        } else if(category.matches("^( |　)+$")) {
			errorMessages.add("カテゴリを入力してください");
		}


        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}