package jp.co.alhinc.hirata_atsushi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import jp.co.alhinc.hirata_atsushi.beans.Branch;
import jp.co.alhinc.hirata_atsushi.beans.Department;
import jp.co.alhinc.hirata_atsushi.beans.User;
import jp.co.alhinc.hirata_atsushi.service.BranchService;
import jp.co.alhinc.hirata_atsushi.service.DepartmentService;
import jp.co.alhinc.hirata_atsushi.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Branch> branches = new BranchService().selectBranches();
		List<Department> departments = new DepartmentService().selectDepartments();
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		List<Branch> branches =  new BranchService().selectBranches();
		List<Department> departments = new DepartmentService().selectDepartments();
		String confirmationPassword = request.getParameter("confirmationpassword");
		User user = getUser(request);

		//値を正しい形式で取得できているか及びアカウント重複チェック
		if ( ! isValid(user, errorMessages,confirmationPassword)) {

			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("management");

	}

	//入力されたユーザーデータを保持するためのメソッド
	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	//値を正しい形式で取得できているか及びアカウント重複チェックのバリデーション用
	private boolean isValid(User user, List<String> errorMessages, String confirmationPassword) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		int branchId = user.getBranchId();
		int departmentId  = user.getDepartmentId();
		User existUser = new UserService().duplicateConfirm(account);

		if(existUser != null) {
			errorMessages.add("アカウントが重複しています");
		}


		if (StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");

		} else if (10 < name.length()) {
			errorMessages.add("名前は10文字以下で入力してください");
		}


		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");

		} else if ((20 < account.length()) || (6 > account.length())) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");

		} else if ( ! account.matches("^[0-9a-zA-Z]+$")) {
			errorMessages.add("アカウント名は半角英数字で入力してください");
		}


		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");

		} else if ((20 < password.length()) || (6 > password.length())) {
			errorMessages.add("パスワードは6文字以上20文字以下で入力してください");

		} else if ( ! password.matches("^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$")) {
			errorMessages.add("パスワードは半角英数字で入力してください");

		} else if ( ! password.equals(confirmationPassword)) {
			errorMessages.add("確認用パスワードが一致しません");
		}

		// 1 ＝ A社、 2 ＝ B社、 3 ＝ C社
		if((branchId == 1) || (branchId == 2) || (branchId == 3)) {

			// 3 ＝ 総務人事部、 4 ＝ 情報管理部
			if((departmentId == 3) || (departmentId == 4)) {
				errorMessages.add("支社と部署の組み合わせが間違っています");
			}
		}

		// 4 ＝ 本社
		if(branchId == 4) {

			// 1 ＝ 営業部、 2 ＝ 技術部
			if((departmentId == 1) || (departmentId == 2)) {
				errorMessages.add("支社と部署の組み合わせが間違っています");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}
