package jp.co.alhinc.hirata_atsushi.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.alhinc.hirata_atsushi.service.UserService;

@WebServlet("/stop")
public class StopServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String changeStatus  = request.getParameter("changeStatus");

		//停止
		if (changeStatus.equals("1")) {
			int updateUserId = Integer.parseInt(request.getParameter("userId"));
			new UserService().updateToStop(updateUserId);
			response.sendRedirect("management");

		//復活
		} else if (changeStatus.equals("0")) {
			int updateUserId = Integer.parseInt(request.getParameter("userId"));
			new UserService().updateToRevival(updateUserId);
			response.sendRedirect("management");
		}
	}
}
