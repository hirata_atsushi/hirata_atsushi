package jp.co.alhinc.hirata_atsushi.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.alhinc.hirata_atsushi.service.CommentService;


@WebServlet(urlPatterns = { "/deletecomment" })
public class DeleteCommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String deleteId = request.getParameter("deleteId");

        new CommentService().delete(deleteId);
        //HomeServletをGETで呼び出し
        response.sendRedirect("./");
    }
}