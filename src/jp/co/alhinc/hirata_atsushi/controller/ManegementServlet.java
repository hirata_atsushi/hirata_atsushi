package jp.co.alhinc.hirata_atsushi.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.alhinc.hirata_atsushi.beans.UserBranchDepartment;
import jp.co.alhinc.hirata_atsushi.service.UserService;

@WebServlet("/management")
public class ManegementServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ユーザー一覧を取得
		List<UserBranchDepartment> userBranchDepartments = 
				new UserService().selectUserBranchDepartments();
		request.setAttribute("userBranchDepartments", userBranchDepartments);
		request.getRequestDispatcher("management.jsp").forward(request, response);
	}
}