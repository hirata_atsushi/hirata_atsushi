package jp.co.alhinc.hirata_atsushi.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import jp.co.alhinc.hirata_atsushi.beans.UserComment;
import jp.co.alhinc.hirata_atsushi.beans.UserMessage;
import jp.co.alhinc.hirata_atsushi.service.CommentService;
import jp.co.alhinc.hirata_atsushi.service.MessageService;

@WebServlet("/")
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Date currentDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String inputCategory = request.getParameter("category");
		String startDate = null;
		String endDate = null;
		List<UserMessage> userMessages = null;


		if( ! StringUtils.isEmpty(request.getParameter("start"))){
			startDate = request.getParameter("start");

		}else if(StringUtils.isEmpty(request.getParameter("start"))) {
			startDate ="2020-01-01";
		}

		if( ! StringUtils.isEmpty(request.getParameter("end"))) {
			endDate = request.getParameter("end");

		} else if ( StringUtils.isEmpty(request.getParameter("end"))){
			endDate = sdf.format(currentDate);
		}


		try {
			userMessages = new MessageService().selectMessages(startDate,endDate,inputCategory);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<UserComment> userComments = new CommentService().selectComments();

		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("inputCategory", inputCategory);
		request.setAttribute("userMessages", userMessages);
		request.setAttribute("userComments", userComments);
        request.getRequestDispatcher("home.jsp").forward(request, response);
	}
}