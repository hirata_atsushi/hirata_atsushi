package jp.co.alhinc.hirata_atsushi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import jp.co.alhinc.hirata_atsushi.beans.Branch;
import jp.co.alhinc.hirata_atsushi.beans.Department;
import jp.co.alhinc.hirata_atsushi.beans.User;
import jp.co.alhinc.hirata_atsushi.beans.UserBranchDepartment;
import jp.co.alhinc.hirata_atsushi.service.BranchService;
import jp.co.alhinc.hirata_atsushi.service.DepartmentService;
import jp.co.alhinc.hirata_atsushi.service.UserService;


@WebServlet("/setting")
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();
		List<Branch> branches = new BranchService().selectBranches();
		List<Department> departments = new DepartmentService().selectDepartments();
		List<UserBranchDepartment> userBranchDepartments = new UserService().selectUserBranchDepartments();


		//URL直打ち対策。GET送信で受け取ったuserIdのチェック。
		if( StringUtils.isEmpty(request.getParameter("userId"))) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./management");
			return;
		}

		if( ! (request.getParameter("userId")).matches("^[0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./management");
			return;
		}

		String userId = request.getParameter("userId");
		User user = new UserService().selectUser(Integer.parseInt(userId));

		if(user == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./management");
			return;
		}

		request.setAttribute("userBranchDepartments", userBranchDepartments);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.setAttribute("user", user);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		List<Branch> branches =  new BranchService().selectBranches();
		List<Department> departments = new DepartmentService().selectDepartments();
		List<UserBranchDepartment> userBranchDepartments = new UserService().selectUserBranchDepartments();


		User user = getUserData(request);
		String confirmationPassword = request.getParameter("confirmationpassword");
		int userId = Integer.parseInt(request.getParameter("userId"));
		user.setId(userId);


		//値を正しい形式で取得できているか及び重複チェック
		if (!isValid(user, errorMessages ,confirmationPassword)) {

			request.setAttribute("userBranchDepartments", userBranchDepartments);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		//ユーザーデータを更新
		new UserService().update(user);
		response.sendRedirect("management");

	}

	//入力された値を保持するためのメソッド
	private User getUserData(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	//値を正しい形式で取得できているか及び重複チェックのバリデーション
	private boolean isValid(User updateUser, List<String> errorMessages, String confirmationPassword) {

		String name = updateUser.getName();
		String account = updateUser.getAccount();
		String password = updateUser.getPassword();
		int branchId = updateUser.getBranchId();
		int departmentId  = updateUser.getDepartmentId();
		int id = updateUser.getId();
		User existUser = new UserService().duplicateConfirm(account);


		if(existUser != null) {
			if(existUser.getId() != id ) {
				errorMessages.add("アカウントが重複しています");
			}
		}


		if (StringUtils.isEmpty(name)) {
			errorMessages.add("名前を入力してください");

		} else if (10 < name.length()){
			errorMessages.add("名前は10文字以下で入力してください");
		}


		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");

		} else if ((20 < account.length()) || (6 > account.length())) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");

		} else if ( ! account.matches("^[0-9a-zA-Z]+$")) {
			errorMessages.add("アカウント名は半角英数字で入力してください");
		}


		if( ! StringUtils.isEmpty(password)) {

			if ((20 < password.length()) || (6 > password.length())) {
			errorMessages.add("パスワードは6文字以上20文字以下で入力してください");

			} else if ( ! password.matches("^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$")) {
			errorMessages.add("パスワードは半角英数字で入力してください");

			} else if ( ! password.equals(confirmationPassword)) {
			errorMessages.add("確認用パスワードが一致しません");
			}
		}

		// 1 ＝ A社、 2 ＝ B社、 3 ＝ C社
		if((branchId == 1) || (branchId == 2) || (branchId == 3)) {

			// 3 ＝ 総務人事部、 4 ＝ 情報管理部
			if((departmentId == 3) || (departmentId == 4)) {
				errorMessages.add("支社と部署の組み合わせが間違っています");
			}
		}

		// 4 ＝ 本社
		if(branchId == 4) {

			// 1 ＝ 営業部、 2 ＝ 技術部
			if((departmentId == 1) || (departmentId == 2)) {
				errorMessages.add("支社と部署の組み合わせが間違っています");
			}
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}