package jp.co.alhinc.hirata_atsushi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import jp.co.alhinc.hirata_atsushi.beans.Comment;
import jp.co.alhinc.hirata_atsushi.beans.User;
import jp.co.alhinc.hirata_atsushi.service.CommentService;


@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//sessionを取得。更にsessionに保存された属性値(loginUser)を取得。
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");
        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");
        String messageId =  request.getParameter("messageId");

        //受け取ったtextパラメーターの中身の有無及び文字数チェック
        if (!isValid(text, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        Comment comment = new Comment();

        comment.setText(text);
        comment.setUserId(user.getId());
        comment.setMessageId(Integer.parseInt(messageId));

        //入力されたコメントをDBに反映
        new CommentService().insert(comment);
        response.sendRedirect("./");
    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isEmpty(text)) {
            errorMessages.add("コメントを入力してください");

        } else if (500 < text.length()) {
            errorMessages.add("コメントは500文字以下で入力してください");

        } else if(text.matches("^( |　)+$")) {
			errorMessages.add("コメントを入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}