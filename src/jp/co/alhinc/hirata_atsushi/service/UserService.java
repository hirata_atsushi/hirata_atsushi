package jp.co.alhinc.hirata_atsushi.service;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;
import static jp.co.alhinc.hirata_atsushi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.alhinc.hirata_atsushi.beans.User;
import jp.co.alhinc.hirata_atsushi.beans.UserBranchDepartment;
import jp.co.alhinc.hirata_atsushi.dao.UserBranchDepartmentDao;
import jp.co.alhinc.hirata_atsushi.dao.UserDao;
import jp.co.alhinc.hirata_atsushi.exception.SQLRuntimeException;
import jp.co.alhinc.hirata_atsushi.utils.CipherUtil;

public class UserService {

	//ユーザー情報登録用
	public void insert(User user) {

		Connection connection = null;
		try {
			//パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().insert(connection, user);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}

	}

	//ユーザー情報変更用
	public void update(User updateUser) {

		Connection connection = null;
		try {
			// パスワード暗号化
			if ( ! StringUtils.isEmpty(updateUser.getPassword())) {
				String encPassword = CipherUtil.encrypt(updateUser.getPassword());
				updateUser.setPassword(encPassword);
			}

			connection = getConnection();
			new UserDao().update(connection, updateUser);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//ユーザー停止用
	public void updateToStop(int userId) {

		Connection connection = null;

		try {

			connection = getConnection();
			new UserDao().updateToStop(connection, userId);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//ユーザー復活用
	public void updateToRevival(int userId) {

		Connection connection = null;

		try {

			connection = getConnection();
			new UserDao().updateToRevival(connection, userId);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}


	//ユーザー一覧取得用
	public List<UserBranchDepartment> selectUserBranchDepartments() {

		Connection connection = null;

		try {

			connection = getConnection();
			List<UserBranchDepartment> userBranchDepartments =
					new UserBranchDepartmentDao().selectUserBranchDepartments(connection);
			commit(connection);
			return userBranchDepartments;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//ログイン時の照合用
	public User selectToLogin(String account, String password) throws SQLRuntimeException {

		Connection connection = null;

		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(password);

			connection = getConnection();
			User user = new UserDao().selectToLogin(connection, account, encPassword);
			commit(connection);
			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//アカウント重複チェック用
	public User duplicateConfirm(String account) {

		Connection connection = null;

		try {

			connection = getConnection();
			User user = new UserDao().selectExistingUser(connection, account);
			commit(connection);
			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//setting画面の入力フォームにユーザーデータを表示させる用
	public User selectUser(int userId) {

		Connection connection = null;

		try {

			connection = getConnection();
			User user = new UserDao().selectUser(connection, userId);
			commit(connection);
			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
}