package jp.co.alhinc.hirata_atsushi.service;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;
import static jp.co.alhinc.hirata_atsushi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.Branch;
import jp.co.alhinc.hirata_atsushi.dao.BranchDao;

public class BranchService {

	public List<Branch> selectBranches() {

		Connection connection = null;

		try {

			connection = getConnection();
			List<Branch> branches = new BranchDao().selectBranches(connection);
			commit(connection);

			return branches;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
}