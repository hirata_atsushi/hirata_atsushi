package jp.co.alhinc.hirata_atsushi.service;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;
import static jp.co.alhinc.hirata_atsushi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.Department;
import jp.co.alhinc.hirata_atsushi.dao.DepartmentDao;

public class DepartmentService {

	public List<Department> selectDepartments() {

		Connection connection = null;

		try {

			connection = getConnection();
			List<Department> departments = new DepartmentDao().selectDepartments(connection);
			commit(connection);

			return departments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}