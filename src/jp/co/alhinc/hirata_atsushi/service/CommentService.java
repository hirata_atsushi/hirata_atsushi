package jp.co.alhinc.hirata_atsushi.service;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;
import static jp.co.alhinc.hirata_atsushi.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.Comment;
import jp.co.alhinc.hirata_atsushi.beans.UserComment;
import jp.co.alhinc.hirata_atsushi.dao.CommentDao;
import jp.co.alhinc.hirata_atsushi.dao.UserCommentDao;

public class CommentService {

	//コメント登録用
	public void insert(Comment comment) {

		Connection connection = null;

		try {

			connection = getConnection();
			new CommentDao().insert(connection, comment);
			commit(connection);

		} catch (RuntimeException e) {

			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//コメント削除用
	public void delete(String deleteId) {

		Connection connection = null;

		try {

			connection = getConnection();
			new CommentDao().delete(connection, deleteId);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//コメント一覧取得
	public List<UserComment> selectComments() {

		Connection connection = null;

		try {

			connection = getConnection();
			List<UserComment> userComments = new UserCommentDao().selectComments(connection);
			commit(connection);

			return userComments;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
}