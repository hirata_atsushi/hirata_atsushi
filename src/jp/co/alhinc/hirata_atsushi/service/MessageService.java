package jp.co.alhinc.hirata_atsushi.service;

import static jp.co.alhinc.hirata_atsushi.utils.CloseableUtil.*;
import static jp.co.alhinc.hirata_atsushi.utils.DBUtil.*;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jp.co.alhinc.hirata_atsushi.beans.Message;
import jp.co.alhinc.hirata_atsushi.beans.UserMessage;
import jp.co.alhinc.hirata_atsushi.dao.MessageDao;
import jp.co.alhinc.hirata_atsushi.dao.UserMessageDao;

public class MessageService {

	//投稿データのDBへの保存用
	public void insert(Message message) {

		Connection connection = null;

		try {

			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//投稿削除用
	public void delete(String deleteId) {

		Connection connection = null;

		try {

			connection = getConnection();
			new MessageDao().delete(connection, deleteId);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	//メッセージ一覧取得用
	public List<UserMessage> selectMessages(String startDate, String endDate, String category) throws ParseException {

		Connection connection = null;

		try {

			connection = getConnection();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			Date start = sdf.parse(startDate);
			Date end = sdf.parse(endDate);
			startDate = sdf.format(start);
			endDate = sdf.format(end);

			startDate = startDate + " 00:00:00";
			endDate =  endDate + " 23:59:59";

			if(category == null) {
				category = "";
			}

			List<UserMessage> userMessages =
					new UserMessageDao().selectMessages(connection,startDate,endDate,category);
			commit(connection);
			return userMessages;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} catch (ParseException e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
}