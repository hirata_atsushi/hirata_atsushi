package jp.co.alhinc.hirata_atsushi.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {

		List<String> errorMessages = new ArrayList<String>();

		try {

			String url = ((HttpServletRequest) request).getServletPath();
			HttpSession session = ((HttpServletRequest) request).getSession();

			if ( ! (url.equals("/login"))) {

				if (session.getAttribute("loginUser") == null) {
					errorMessages.add("ログインしてください");
					session.setAttribute("errorMessages", errorMessages);
					((HttpServletResponse) response).sendRedirect("./login");
					return;
				}
			}
			chain.doFilter(request, response);

		} catch (ServletException se) {
		} catch (IOException e) {
		}
	}

	//フィルタの初期化処理に使う
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	//フィルタの破棄時に呼び出す
	public void destroy() {
	}
}