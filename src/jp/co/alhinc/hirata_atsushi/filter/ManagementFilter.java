package jp.co.alhinc.hirata_atsushi.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.alhinc.hirata_atsushi.beans.User;
import jp.co.alhinc.hirata_atsushi.service.UserService;



@WebFilter(urlPatterns = {"/signup","/management","/setting"})
public class ManagementFilter implements Filter {




	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();

		User user = (User) session.getAttribute("loginUser");
		User selectUser = new UserService().selectUser(user.getId());
		String departmentId = String.valueOf(selectUser.getDepartmentId());

		//総務人事部のIDは3で設定。
		if( ! departmentId.equals("3")) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("管理者以外はアクセスできません");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse) response).sendRedirect("./");

		} else {
			chain.doFilter(request, response);
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

	public void destroy() {
	}
}