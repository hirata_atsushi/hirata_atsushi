<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>


		<form action="signup" method="post"><br />

			<label for="name">名前</label>
				<input name="name" id="name" value="${user.name}"/><br />

			<label for="account">アカウント名</label>
				<input name="account"id="account" value="${user.account}"/><br />

			<label for="password">パスワード</label>
				<input name="password" type="password" id="password" /><br />

			<label for="confirmationpassword">確認用パスワード</label>
				<input name="confirmationpassword" type="password" id="confirmationpassword" /><br />

			<label for="branchlist">支社</label>
				<select id="branchId" name="branchId">
					<c:forEach items="${branches}" var="branch">
						<option value="${branch.id}" ${user.branchId  == branch.id ? 'selected' : ''}>${branch.name}</option>
					</c:forEach>
				</select><br />

			<label for="departmentlist">部署</label>
				<select id="departmentId" name="departmentId">
					<c:forEach items="${departments}" var="department">
						<option value="${department.id}" ${user.departmentId  == department.id ? 'selected' : ''}>${department.name}</option>
					</c:forEach>
				</select><br />

			<input type="submit" value="登録" /><br />

			<a href="management">戻る</a>

		</form>

		<div class="copyright">Copyright(c)hirata atsushi</div>
	</div>
</body>
</html>