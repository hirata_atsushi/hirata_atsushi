<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>home</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
				<a href="./">ホーム</a>
				<a href="message">新規投稿</a>
			<c:if test="${ loginUser.departmentId == 3 }">
				<a href="management">ユーザー管理</a>
			</c:if>
				<a href="logout">ログアウト</a>
		</div>


	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
    		<ul>
        		<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
    		</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>


	<form action="./" method="get">
	
		<label for="date">日付</label>
			<input type="date" id="start" name="start" min="2000-01-01" max="2099-12-31" value="${startDate}">～
			<input type="date" id="end" name="end" min="2000-01-01" max="2099-12-31" value="${endDate}"><br>

		<label for="category">カテゴリ</label>
			<input type="text" id="category" name="category" value="${inputCategory}" >
			<input type="submit" value="絞り込み" />
	</form>


	<div class="userMessages">
		<c:if test="${ not empty loginUser }">
			<c:forEach items="${userMessages}" var="userMessage">
				<div class="message">
					<div class="messageveiw" style="padding: 10px; margin-bottom: 10px; border: 5px double #333333;">
						<div class="name">
							<span class="name">【投稿者】<c:out value="${userMessage.userName}" /></span>
							<span class="title">【件名】<c:out value="${userMessage.title}" /></span>
							<span class="category">【カテゴリ】<c:out value="${userMessage.category}" /></span>
						</div>

						<div class="text">
							<br>
							<c:forEach items="${userMessage.getSplitedText()}" var="splittext">
								<b><c:out value="${splittext}" /></b><br/>
							</c:forEach>
							<br>
						</div>

						<div class="date">
							<fmt:formatDate value="${userMessage.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
					</div>

					<div class="delete">
						<c:if test="${userMessage.userId == loginUser.id }">
							<form action="deletemessage " method="post" onSubmit="return check()">
								<input type="hidden" name="deleteId" value="<c:out value="${userMessage.id}"/>"><br />
								<input type="submit" value="投稿削除" /><br />
							</form>
						</c:if>
					</div>

					<br>

					<c:forEach items="${userComments}" var="userComment">
						<div class="userComment">
							<c:if test="${ userMessage.id == userComment.messageId }">

								<label for="name">【名前】</label>
									<c:out value="${userComment.userName}" /><br>

								<label for="content">【投稿内容】</label>
									<c:forEach items="${userComment.getSplitedText()}" var="splitcomment">
										<c:out value="${splitcomment}" /><br>
									</c:forEach><br>

								<label for="date">【投稿日時】</label>
									<fmt:formatDate value="${userComment.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" /><br>

								<form action="deletecomment" method="post" onSubmit="return check()">
									<c:if test="${userComment.userId == loginUser.id }">
										<input type="submit" value="コメント削除" /><br>
										<input type="hidden" name="deleteId" value="<c:out value="${userComment.id}"/>"><br>
									</c:if>
								</form>

							</c:if>
						</div>
					</c:forEach>

					<form action="comment" method="post">

						<label for="text">コメント</label>
							<textarea name="text" cols="35" rows="5" id="text">${text}</textarea>
							<input type="submit" value="コメント投稿" /><br />
							<input type="hidden" name="messageId" value="<c:out value="${userMessage.id}"/>"><br />

					</form>

				</div>
			</c:forEach>

		</c:if>
	</div>
	</div>

	<div class="copyright">Copyright(c)hirata atsushi</div>

<script type="text/javascript">
<!--

function check(){
	if(confirm("削除してよろしいですか？")){
		alert("削除されました");
		return true;
	}
	else{
		alert("キャンセルされました");
		return false;
	}
}
//-->
</script>
</body>
</html>