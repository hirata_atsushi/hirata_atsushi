<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>home</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
				<a href="./">ホーム</a>
				<a href="signup">ユーザー新規登録</a>
		</div>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
    		<ul>
        		<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
    		</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>



		<div class = userlist>
			<c:forEach items="${userBranchDepartments}" var="userInfo"><br>
	    		<div class="userlistveiw" style="padding: 10px; margin-bottom: 10px; border: 5px double #333333;">

					<label for="account">【アカウント】</label>
						<c:out value="${userInfo.account}" /><br>

					<label for="name">【名前】</label>
						<c:out value="${userInfo.userName}" /><br>

					<label for="branch">【支社】</label>
						<c:out value="${userInfo.branchName}" /><br>

					<label for="department">【部署】</label>
						<c:out value="${userInfo.departmentName}" /><br>

					<label for="statas">【ステータス】</label>
						<c:if test="${userInfo.isStopped == 0}">活動中</c:if>
						<c:if test="${userInfo.isStopped == 1}">停止中</c:if><br>

					<form action="setting" method="get" >
						<input type="submit" value="編集画面" />
						<input type="hidden" name="userId" value="<c:out value="${userInfo.id}"/>">
					</form>

				    <c:if test="${loginUser.id != userInfo.id}">
						<c:if test="${userInfo.isStopped == 0}">
						<form action="stop" method="post" onSubmit="return stopCheck()">
						<input type="submit" value="停止" />
						<input type="hidden" name="userId" value="<c:out value="${userInfo.id}"/>">
						<input type="hidden" name="changeStatus" value="1"/>
						</form>
						</c:if>
					</c:if>

					<c:if test="${userInfo.isStopped == 1}">
					<form action="stop" method="post" onSubmit="return revivalCheck()">
						<input type="submit" value="復活" />
						<input type="hidden" name="userId" value="<c:out value="${userInfo.id}"/>">
						<input type="hidden" name="changeStatus" value="0"/>
					</form>
					</c:if>

				</div>
			</c:forEach>
		</div>
	</div>

	<div class="copyright">Copyright(c)hirata atsushi</div>

<script type="text/javascript">
<!--
function stopCheck(){
	if(confirm("停止してよろしいですか？")){
		alert("停止しました");
		return true;
	}
	else{
		alert("キャンセルされました");
		return false;
	}
}

function revivalCheck(){
	if(confirm("復活させてよろしいですか？")){
		alert("復活させました");
		return true;
	}
	else{
		alert("キャンセルされました");
		return false;
	}
}
//-->
</script>

</body>
</html>